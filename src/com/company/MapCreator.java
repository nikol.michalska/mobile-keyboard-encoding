package com.company;

import static com.company.Main.phoneKeyboard;

public class MapCreator {
    public static void createData(){
        phoneKeyboard.put("A", "2");
        phoneKeyboard.put("B", "22");
        phoneKeyboard.put("C", "222");
        phoneKeyboard.put("D", "3");
        phoneKeyboard.put("E", "33");
        phoneKeyboard.put("F", "333");
        phoneKeyboard.put("G", "4");
        phoneKeyboard.put("H", "44");
        phoneKeyboard.put("I", "444");
        phoneKeyboard.put("J", "5");
        phoneKeyboard.put("K", "55");
        phoneKeyboard.put("L", "555");
        phoneKeyboard.put("M", "6");
        phoneKeyboard.put("N", "66");
        phoneKeyboard.put("O", "666");
        phoneKeyboard.put("P", "7");
        phoneKeyboard.put("R", "77");
        phoneKeyboard.put("S", "777");
        phoneKeyboard.put("T", "8");
        phoneKeyboard.put("U", "88");
        phoneKeyboard.put("V", "888");
        phoneKeyboard.put("W", "9");
        phoneKeyboard.put("X", "99");
        phoneKeyboard.put("Y", "999");
        phoneKeyboard.put("Z", "0");
    }
}
