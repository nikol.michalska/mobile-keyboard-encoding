package com.company;

import java.util.HashMap;
import java.util.Objects;
import java.util.Scanner;

import static com.company.MapCreator.createData;

public class Main {
    static HashMap<String, String> phoneKeyboard = new HashMap<>();
    static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {
        createData();
        askUserToEncode();


    }

    public static String fromKeyboard(String letter) {
        return phoneKeyboard.getOrDefault(letter, "");
    }

    public static String encode(String word) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (Objects.nonNull(word)) {
            for (int a = 0; a < word.length(); a++) {
                String letter = String.valueOf(word.charAt(a));
                stringBuilder.append(fromKeyboard(letter));
            }
        }
        return stringBuilder.toString();
    }

    public static void askUserToEncode() {
        String doesUserWantToEncode;
        do {
            System.out.println("Put the word");
            String wordToEncode = scanner.next();
            encode(wordToEncode);
            System.out.println(encode(wordToEncode));
            System.out.println("Do you want to encode the other word?");
            doesUserWantToEncode = scanner.next();
        } while (!doesUserWantToEncode.equalsIgnoreCase("no"));

    }
}
